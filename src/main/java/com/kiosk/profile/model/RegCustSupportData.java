/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
public class RegCustSupportData {
    @Size(max = 255)
    private String warrantyInfo;
    @Size(max = 255)
    private String returnPolicy;
    @Size(max = 255)
    private String customerCare;

    /**
     * @return the warrantyInfo
     */
    public String getWarrantyInfo() {
        return warrantyInfo;
    }

    /**
     * @param warrantyInfo the warrantyInfo to set
     */
    public void setWarrantyInfo(String warrantyInfo) {
        this.warrantyInfo = warrantyInfo;
    }

    /**
     * @return the returnPolicy
     */
    public String getReturnPolicy() {
        return returnPolicy;
    }

    /**
     * @param returnPolicy the returnPolicy to set
     */
    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    /**
     * @return the customerCare
     */
    public String getCustomerCare() {
        return customerCare;
    }

    /**
     * @param customerCare the customerCare to set
     */
    public void setCustomerCare(String customerCare) {
        this.customerCare = customerCare;
    }
    
}
