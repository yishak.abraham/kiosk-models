/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import javax.ejb.Remove;
import javax.ejb.Stateful;


/**
 *
 * @author TheStig
 */
@Stateful
public class RegistrationValidationModel{

    private String validPhone;
    private String validEmail;
    private String validUrl;
    private int phoneCode;
    private String validationCode;
    private boolean validated;

    /**
     * @return the validPhone
     */
    public String getValidPhone() {
        return validPhone;
    }

    /**
     * @param validPhone the validPhone to set
     */
    public void setValidPhone(String validPhone) {
        this.validPhone = validPhone;
    }

    /**
     * @return the validEmail
     */
    public String getValidEmail() {
        return validEmail;
    }

    /**
     * @param validEmail the validEmail to set
     */
    public void setValidEmail(String validEmail) {
        this.validEmail = validEmail;
    }

    /**
     * @return the validUrl
     */
    public String getValidUrl() {
        return validUrl;
    }

    /**
     * @param validUrl the validUrl to set
     */
    public void setValidUrl(String validUrl) {
        this.validUrl = validUrl;
    }

    /**
     * @return the phoneCode
     */
    public int getPhoneCode() {
        return phoneCode;
    }

    /**
     * @param phoneCode the phoneCode to set
     */
    public void setPhoneCode(int phoneCode) {
        this.phoneCode = phoneCode;
    }

    @Remove
    public void cancel() {
    }

    /**
     * @return the validationCode
     */
    public String getValidationCode() {
        return validationCode;
    }

    /**
     * @param validationCode the validationCode to set
     */
    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    /**
     * @return the validated
     */
    public boolean isValidated() {
        return validated;
    }

    /**
     * @param validated the validated to set
     */
    public void setValidated(boolean validated) {
        this.validated = validated;
    }
}
