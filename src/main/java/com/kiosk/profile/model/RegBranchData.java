/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;


import java.util.Collection;
import java.util.Set;
import javax.validation.Valid;

import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
public class RegBranchData {
    @Size(max = 20)
    private String branchName;
    @Valid
    private RegLocationData location;
    private Set<RegPhone> phoneData;

    /**
     * @return the branchNumber
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchNumber to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }
    
    /**
     * @return the location
     */
    public RegLocationData getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(RegLocationData location) {
        this.location = location;
    }

    /**
     * @return the phoneData
     */
    public Set<RegPhone> getPhoneData() {
        return phoneData;
    }

    /**
     * @param phoneData the phoneData to set
     */
    public void setPhoneData(Set<RegPhone> phoneData) {
        this.phoneData = phoneData;
    }


}
