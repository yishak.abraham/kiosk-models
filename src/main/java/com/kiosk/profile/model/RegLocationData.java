/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */

public class RegLocationData {
    
    @Size(max = 30)
    private String country;
    @Size(max = 30)
    private String region;
    @Size(max = 30)
    private String city;
    @Size(max = 30)
    private String sub_city;
    private String buildingAndRoomNo;
    //private GPS gps;
    private String latitude;
    private String longitude;

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the sub_city
     */
    public String getSub_city() {
        return sub_city;
    }

    /**
     * @param sub_city the sub_city to set
     */
    public void setSub_city(String sub_city) {
        this.sub_city = sub_city;
    }

    /**
     * @return the buildingAndRoomNo
     */
    public String getBuildingAndRoomNo() {
        return buildingAndRoomNo;
    }

    /**
     * @param buildingAndRoomNo the buildingAndRoomNo to set
     */
    public void setBuildingAndRoomNo(String buildingAndRoomNo) {
        this.buildingAndRoomNo = buildingAndRoomNo;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
