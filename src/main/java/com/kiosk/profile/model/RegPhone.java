/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
    public class RegPhone {
    private int choice;
    @NotEmpty
    @Size(max = 20)
    private String newPhoneNumber;
//    private String existingPhoneNumber;
    private int id;

    /**
     * @return the choice
     */
    public int getChoice() {
        return choice;
    }

    /**
     * @param choice the choice to set
     */
    public void setChoice(int choice) {
        this.choice = choice;
    }

    /**
     * @return the newPhoneNumber
     */
    public String getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * @param newPhoneNumber the newPhoneNumber to set
     */
    public void setNewPhoneNumber(String newPhoneNumber) {
        this.newPhoneNumber = newPhoneNumber;
    }

//    /**
//     * @return the existingPhoneNumber
//     */
//    public String getExistingPhoneNumber() {
//        return existingPhoneNumber;
//    }
//
//    /**
//     * @param existingPhoneNumber the existingPhoneNumber to set
//     */
//    public void setExistingPhoneNumber(String existingPhoneNumber) {
//        this.existingPhoneNumber = existingPhoneNumber;
//    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
