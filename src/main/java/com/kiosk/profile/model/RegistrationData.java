/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import java.util.ArrayList;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author TheStig
 */
public class RegistrationData {

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email.trim();
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the storeUrl
     */
    public String getStoreUrl() {
        return storeUrl;
    }

    /**
     * @param storeUrl the storeUrl to set
     */
    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName the storeName to set
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the categories
     */
    public ArrayList<String> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    /**
     * @return the registeredBy
     */
    public String getRegisteredBy() {
        return registeredBy;
    }

    /**
     * @param registeredBy the registeredBy to set
     */
    public void setRegisteredBy(String registeredBy) {
        this.registeredBy = registeredBy;
    }
    

    @Size(max = 20)
    private String phoneNumber;
    @Email
    private String email;
    @NotEmpty
    @Size(max = 20)
    private String firstName;
    @NotEmpty
    @Size(max = 20)
    private String lastName;
    @NotEmpty
    @Size(max = 15)
    private String storeUrl;
    @NotEmpty
    @Size(max = 30)
    private String storeName;
    @NotEmpty
    private ArrayList<String> categories;
    @NotNull
    private String registeredBy;
    @NotNull
    @Size(min=6)
    private String password;
    @NotNull
    @Size(min=6)
    private String confirmPassword;

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the confirmPassword
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * @param confirmPassword the confirmPassword to set
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    

}
