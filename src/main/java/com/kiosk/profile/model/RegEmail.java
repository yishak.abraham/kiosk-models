/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;


/**
 *
 * @author Isaac
 */
public class RegEmail {
    private int choice;
    @Email
    @NotEmpty
    private String newEmail;
    private Long id;
//    private String existingEmail;

    /**
     * @return the choice
     */
    public int getChoice() {
        return choice;
    }

    /**
     * @param choice the choice to set
     */
    public void setChoice(int choice) {
        this.choice = choice;
    }

    /**
     * @return the newEmail
     */
    public String getNewEmail() {
        return newEmail;
    }

    /**
     * @param newEmail the newEmail to set
     */
    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

//    /**
//     * @return the existingEmail
//     */
//    public String getExistingEmail() {
//        return existingEmail;
//    }
//
//    /**
//     * @param existingEmail the existingEmail to set
//     */
//    public void setExistingEmail(String existingEmail) {
//        this.existingEmail = existingEmail;
//    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
}
