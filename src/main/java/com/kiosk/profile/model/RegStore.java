/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.validation.Valid;
import javax.validation.constraints.Size;

/**
 *
 * @author sir Isaac featuring sir mike
 */
public class RegStore {

    /**
     * @return the emailData
     */
    public Collection<RegEmail> getEmailData() {
        return emailData;
    }

    /**
     * @param emailData the emailData to set
     */
    public void setEmailData(Collection<RegEmail> emailData) {
        this.emailData = emailData;
    }

    /**
     * @return the phoneData
     */
    public Collection<RegPhone> getPhoneData() {
        return phoneData;
    }

    /**
     * @param phoneData the phoneData to set
     */
    public void setPhoneData(Collection<RegPhone> phoneData) {
        this.phoneData = phoneData;
    }
    @Size(max = 30)
    private String storeName;
    @Size(max = 15)
    private String storeUrl;
    private String description;
    @Size(max = 100)
    private String locationDescription;
    private String expertise;
    private ArrayList<Integer> categories;
    @Valid
    private RegLocationData location;
    @Valid
    private Collection<RegEmail> emailData;
    @Valid
    private Collection<RegPhone> phoneData;
        /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName the storeName to set
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the storeUrl
     */
    public String getStoreUrl() {
        return storeUrl;
    }

    /**
     * @param storeUrl the storeUrl to set
     */
    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the locationDescription
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * @param locationDescription the locationDescription to set
     */
    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    /**
     * @return the expertise
     */
    public String getExpertise() {
        return expertise;
    }

    /**
     * @param expertise the expertise to set
     */
    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    /**
     * @return the categories
     */
    public ArrayList<Integer> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }

    /**
     * @return the location
     */
    public RegLocationData getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(RegLocationData location) {
        this.location = location;
    }
}
