/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author TheStig
 */
@NamedQueries({

@NamedQuery(name = "Phone.findByDigit",
        query = "SELECT p FROM Phone p WHERE p.PhoneNumber=:pn"),
    @NamedQuery(name = "Phone.usedForRegistration",
            query = "SELECT p.PhoneNumber FROM Phone p WHERE P.PhoneNumber =:pn AND "+
                    "p.isUsedForRegistration = true"),
 

})
@Entity
public class Phone implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name="phone_gen",sequenceName="phone_seq",
            initialValue=1,allocationSize=1)
    @GeneratedValue(generator="phone_gen")
    private int id;
    @NotEmpty
    private String PhoneNumber;
    private boolean isUsedForRegistration;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the PhoneNumber
     */
    public String getPhoneNumber() {
        return PhoneNumber;
    }

    /**
     * @param PhoneNumber the PhoneNumber to set
     */
    public void setPhoneNumber(String PhoneNumber) {
        this.PhoneNumber = PhoneNumber;
    }

    /**
     * @return the isUsedForRegistration
     */
    public boolean isIsUsedForRegistration() {
        return isUsedForRegistration;
    }

    /**
     * @param isUsedForRegistration the isUsedForRegistration to set
     */
    public void setIsUsedForRegistration(boolean isUsedForRegistration) {
        this.isUsedForRegistration = isUsedForRegistration;
    }
    
    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(!(o instanceof Phone))
            return false;
        Phone p = (Phone)o;
        return p.id == this.id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + this.id;
        return hash;
    }

}
