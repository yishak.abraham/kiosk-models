/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import com.kiosk.image.Image;
import com.kiosk.inventory.entity.Item;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author TheStig
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Store.findAll",
            query = "SELECT s FROM Store s"),
    @NamedQuery(name = "Store.findById",
            query = "SELECT s FROM Store s WHERE s.storeId = :sid"),
    @NamedQuery(name = "Store.findByUrl",
            query = "SELECT s FROM Store s WHERE LOWER(s.storeUrl)=LOWER(:sUrl)"),
    @NamedQuery(name = "Store.findByStoreName",
            query = "SELECT s FROM Store s WHERE s.storeName=:sName"),
    @NamedQuery(name = "Store.findStoreUrlByEmail",
            query = "SELECT s.storeUrl FROM Store s WHERE s.owner.email.email =:email")
})
public class Store implements Serializable, StoreInterface {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "store_gen", sequenceName = "store_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "store_gen")
    protected int storeId;
    @NotEmpty(message = "store name cannot be left empty")
    @Size(max = 30)
    protected String storeName;
    @Column(columnDefinition = "tsvector",insertable = false,updatable = false)
    private String storeNameToken;
    @NotEmpty(message = "store url cannot be left empty")
    @Size(max = 20)
    protected String storeUrl;
    private boolean verified;
    @Column(length=12800)
    protected String description;
    @Enumerated(EnumType.STRING)
    protected StoreStatus.Status status;
    @OneToMany( fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Phone> phones;
    @OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Email> emails;
    @Size(max = 100)
    private String locationDescription;
    @Valid
    @OneToOne(mappedBy = "store")
    private StoreOwner owner;
    @ManyToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    private List<Category> categories;
    @Column(length=12800)
    private String expertise;
    @Valid
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Location location;
    @OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "store")
    private Set<Branch> branches;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private CustomerSupport customerSupport;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Theme theme;
    @OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Layout> layout;
    @JsonbTransient
    @OneToMany(mappedBy = "store", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Collection<Item> items;
    @OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @Fetch(FetchMode.SUBSELECT)
    private final List<Image> storeImage;
    private boolean isLocked;
    private int token;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime createdDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime featuredPeriod;
    @JsonbTransient
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, mappedBy = "StoreId")
    private Collection<StoreActivity> storeActivity;

    public Store() {
        this.storeImage = new ArrayList<>();
        this.storeActivity = new HashSet<>();
    }

    /**
     * @return the storeId
     */
    public int getStoreId() {
        return storeId;
    }

    /**
     * @param storeId the storeId to set
     */
    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    /**
     * @return the storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * @param storeName the storeName to set
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     * @return the storeUrl
     */
    public String getStoreUrl() {
        return storeUrl;
    }

    /**
     * @param storeUrl the storeUrl to set
     */
    public void setStoreUrl(String storeUrl) {
        this.storeUrl = storeUrl;
    }

    /**
     * @return the verified
     */
    public boolean isVerified() {
        return verified;
    }

    /**
     * @param verified the verified to set
     */
    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    public StoreStatus.Status getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(StoreStatus.Status status) {
        this.status = status;
    }

    /**
     * @return the phones
     */
    @Override
    public Set<Phone> getPhones() {
        return phones;
    }

    /**
     * @param phones the phones to set
     */
    @Override
    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    /**
     * @return the emails
     */
    @Override
    public Set<Email> getEmails() {
        return emails;
    }

    /**
     * @param emails the emails to set
     */
    @Override
    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }

    /**
     * @return the locationDescription
     */
    public String getLocationDescription() {
        return locationDescription;
    }

    /**
     * @param locationDescription the locationDescription to set
     */
    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Store)) {
            return false;
        }
        final Store other = (Store) obj;
        return this.storeId == other.storeId;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.storeId);
        return hash;
    }

    /**
     * @return the owner
     */
    public StoreOwner getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(StoreOwner owner) {
        this.owner = owner;
    }

    /**
     * @return the categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * @param categories the categories to set
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the branches
     */
    public Collection<Branch> getBranches() {
        return branches;
    }

    /**
     * @param branches the branches to set
     */
    public void setBranches(Set<Branch> branches) {
        this.branches = branches;
    }

    /**
     * @return the cutomerSoprt
     */
    public CustomerSupport getCustomerSupport() {
        return customerSupport;
    }

    /**
     * @param customerSupport the cutomerSoprt to set
     */
    public void setCutomerSupport(CustomerSupport customerSupport) {
        this.customerSupport = customerSupport;
    }

    /**
     * @return the expertise
     */
    public String getExpertise() {
        return expertise;
    }

    /**
     * @param expertise the expertise to set
     */
    public void setExpertise(String expertise) {
        this.expertise = expertise;
    }

    /**
     * @return the theme
     */
    public Theme getTheme() {
        return theme;
    }

    /**
     * @param theme the theme to set
     */
    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    /**
     * @return the layout
     */
    public Collection<Layout> getLayout() {
        return layout;
    }

    /**
     * @param layout the layout to set
     */
    public void setLayout(Set<Layout> layout) {
        this.layout = layout;
    }

    /**
     * @return the items
     */
    public Collection<Item> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Collection<Item> items) {
        this.items = items;
    }

    /**
     * @return the storeImages
     */
    public List<Image> getStoreImage() {
        return storeImage;
    }

    /**
     * @param storeImages the storeImages to set
     */
    public void setStoreImage(Image storeImages) {
        this.storeImage.add(storeImages);
    }

    /**
     * @return the isLocked
     */
    public boolean isIsLocked() {
        return isLocked;
    }

    /**
     * @param isLocked the isLocked to set
     */
    public void setIsLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getFeaturedPeriod() {
        return featuredPeriod;
    }

    public void setFeaturedPeriod(LocalDateTime featuredPeriod) {
        this.featuredPeriod = featuredPeriod;
    }

    public Collection<StoreActivity> getStoreActivity() {
        return storeActivity;
    }

    public void setStoreActivity(StoreActivity storeActivity) {
        this.storeActivity.add(storeActivity);
    }

    public String getNameToken() {
        return storeNameToken;
    }

    public void setNameToken(String storeNameToken) {
        this.storeNameToken = storeNameToken;
    }
}
