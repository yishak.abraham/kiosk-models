/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author TheStig
 */
@Entity

@NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r")
@NamedQuery(name = "Role.findByGroupName", query = "SELECT r FROM Role r WHERE r.groupName = :groupName")
@NamedQuery(name = "Role.findByGroupNames", query = "SELECT r FROM Role r WHERE r.groupName IN :groupNames")
@NamedQuery(name = "Role.findByGroupDesc", query = "SELECT r FROM Role r WHERE r.groupDesc = :groupDesc")

public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Enumerated(EnumType.STRING)
    @Id
    private RoleEnum groupName;
    @Size(max = 200)
    private String groupDesc;
    @JsonbTransient
    @ManyToMany(mappedBy = "roleCollection")
    private Collection<StoreOwner> userCollection;

    public Role() {
    }

    public Role(RoleEnum groupName) {
        this.groupName = groupName;
    }

    public RoleEnum getGroupName() {
        return groupName;
    }

    public void setGroupName(RoleEnum groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }
    @JsonbTransient
    public Collection<StoreOwner> getUserCollection() {
        return userCollection;
    }
    @JsonbTransient
    public void setUserCollection(Collection<StoreOwner> userCollection) {
        this.userCollection = userCollection;
    }

@Override
public boolean equals(Object o){
    if(this == o)
        return true;
    if(!(o instanceof Role))
        return false;
    Role r = (Role) o;
    return this.groupName == r.groupName;
}

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.groupName);
        return hash;
    }

}
