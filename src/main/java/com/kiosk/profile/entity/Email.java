/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author TheStig
 */
@NamedQuery(name = "Email.findByEmail",
        query = "SELECT e FROM Email e WHERE LOWER(e.email) = LOWER(:email)")
@Entity
public class Email implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "email_gen", sequenceName = "email_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "email_gen")
    private Long id;
    @NotEmpty
    private String email;
    private boolean isUsedForRegistration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isIsUsedForRegistration() {
        return isUsedForRegistration;
    }

    public void setIsUsedForRegistration(boolean isUsedForRegistration) {
        this.isUsedForRegistration = isUsedForRegistration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Email)) {
            return false;
        }
        Email e = (Email) o;
        return e.id.equals(this.id);

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }
}
