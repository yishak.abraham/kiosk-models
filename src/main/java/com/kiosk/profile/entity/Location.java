/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
@Entity
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "location_gen", sequenceName = "location_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "location_gen")
    private Long id;
//    private 
    @Size(max = 30)
    private String country = "";
    @Size(max = 30)
    private String region = "";
    @Size(max = 30)
    private String city = "";
    @Size(max = 30)
    private String sub_city = "";
    @Size(max = 15)
    private String buildingAndRoomNo = "";
    //private GPS gps;
    private String latitude = "";
    private String longitude = "";
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Location)) {
            return false;
        }
        Location other = (Location) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bh.kiosk.store.entity.Location[ id=" + id + " ]";
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the region
     */
    public String getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the buildingAndRoomNo
     */
    public String getBuildingAndRoomNo() {
        return buildingAndRoomNo;
    }

    /**
     * @param buildingAndRoomNo the buildingAndRoomNo to set
     */
    public void setBuildingAndRoomNo(String buildingAndRoomNo) {
        this.buildingAndRoomNo = buildingAndRoomNo;
    }

    /**
     * @return the latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     * @return the sub_city
     */
    public String getSub_city() {
        return sub_city;
    }

    /**
     * @param sub_city the sub_city to set
     */
    public void setSub_city(String sub_city) {
        this.sub_city = sub_city;
    }
    
}
