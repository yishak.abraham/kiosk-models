/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import com.kiosk.inventory.entity.Tag;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

/**
 *
 * @author TheStig
 */
@NamedQueries({
    @NamedQuery(name = "Category.findAll",
            query = "SELECT c FROM Category c"),
    @NamedQuery(name = "Category.findByName",
            query = "SELECT c FROM Category c WHERE c.name=:cname"),
    @NamedQuery(name ="Category.findRootCategories",
            query="SELECT c FROM Category c WHERE c.parent IS NUll")
})
@Entity
public class Category implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	/**
     * @return the tags
     */
    public List<Tag> getTags() {
        return tags;
    }

    /**
     * @param tag the tags to set
     */
    public void setTags(Tag tag) {
        this.tags.add(tag);
    }
    @Id
    @SequenceGenerator(name="category_gen",sequenceName="category_seq",
            initialValue=1,allocationSize=1)
    @GeneratedValue(generator="category_gen")
    @Enumerated(EnumType.STRING)
    private CategoryEnum name;
    @OneToOne
    private  Category parent;
    @JsonbTransient
    @OneToMany
    private List<Category>children;
    @JsonbTransient
    @OneToMany(mappedBy="category")
    private List<Tag> tags;
    

    public Category() {
        this.children = new ArrayList<>();
        this.tags = new ArrayList<>();
    }
    
    /**
     * @return the name
     */
    public CategoryEnum getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(CategoryEnum name) {
        this.name = name;
    }

    /**
     * @return the parent
     */
    public Category getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Category parent) {
        this.parent = parent;
    }
    
    @Override
    public boolean equals(Object o){
        if(this == o)
            return true;
        if(!(o instanceof Category))
            return false;
        Category cat = (Category)o;
        return cat.name == this.name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        return hash;
    }   

    /**
     * @return the children
     */
    public List<Category> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(Category children) {
        this.children.add(children);
    }
}
