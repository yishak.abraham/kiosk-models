/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.util.Collection;
import java.util.Set;

/**
 *
 * @author mmesfin
 */
public interface StoreInterface {
    Set<Phone> getPhones();
    void setPhones(Set<Phone> phones);
    Set<Email> getEmails();
    void setEmails(Set<Email> emails);
}
