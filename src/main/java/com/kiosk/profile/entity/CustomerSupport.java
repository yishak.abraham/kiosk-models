/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
@Entity
public class CustomerSupport implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "customer_gen", sequenceName = "customer_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "customer_gen")
    private Long id;
    
    @Column(columnDefinition="text")
    private String warrantyInfo = "";
    @Column(columnDefinition="text")
    private String returnPolicy = "";
    @Column(columnDefinition="text")
    private String customerCare = "";
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerSupport)) {
            return false;
        }
        CustomerSupport other = (CustomerSupport) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bh.kiosk.store.entity.CustumerSupport[ id=" + id + " ]";
    }

    /**
     * @return the warrantyInfo
     */
    public String getWarrantyInfo() {
        return warrantyInfo;
    }

    /**
     * @param warrantyInfo the warrantyInfo to set
     */
    public void setWarrantyInfo(String warrantyInfo) {
        this.warrantyInfo = warrantyInfo;
    }

    /**
     * @return the returnPolicy
     */
    public String getReturnPolicy() {
        return returnPolicy;
    }

    /**
     * @param returnPolicy the returnPolicy to set
     */
    public void setReturnPolicy(String returnPolicy) {
        this.returnPolicy = returnPolicy;
    }

    /**
     * @return the customerCare
     */
    public String getCustomerCare() {
        return customerCare;
    }

    /**
     * @param customerCare the customerCare to set
     */
    public void setCustomerCare(String customerCare) {
        this.customerCare = customerCare;
    }
    
}
