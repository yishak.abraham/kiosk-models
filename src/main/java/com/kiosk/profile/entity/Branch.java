/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 *
 * @author Isaac featuring theStig
 */
@Entity
public class Branch implements Serializable,StoreInterface {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "branch_gen", sequenceName = "branch_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "branch_gen")
    private int id;
    @NotEmpty(message = "branch name cannot be left empty")
    @Size(max = 30)
    private String branchName;
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Set<Phone> phones;
    @Valid
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Location location;
    @JsonbTransient
    @ManyToOne(fetch = FetchType.LAZY)
    private Store store;
     @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime createdDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Branch)) {
            return false;
        }
        Branch other = (Branch) object;
       
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "com.bh.kiosk.store.entity.Branch[ id=" + id + " ]";
    }

    /**
     * @return the branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * @param branchName the branchName to set
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * @return the phones
     */
    @Override
    public Set<Phone> getPhones() {
        return phones;
    }

    /**
     * @param phones the phones to set
     */
    @Override
    public void setPhones(Set<Phone> phones) {
        this.phones = phones;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public Set<Email> getEmails() {
       return null;
    }

    @Override
    public void setEmails(Set<Email> emails) {
       //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the createdDate
     */
    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }


}
