/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

/**
 *
 * @author Isaac
 */
@Entity
public class Layout implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;    
    @Size(max = 20)
    private String layoutName;
    private String cssFileUrl;
    private String jsFileUrl;
            
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    /**
     * @return the cssFileUrl
     */
    public String getCssFileUrl() {
        return cssFileUrl;
    }

    /**
     * @param cssFileUrl the cssFileUrl to set
     */
    public void setCssFileUrl(String cssFileUrl) {
        this.cssFileUrl = cssFileUrl;
    }

    /**
     * @return the jsFileUrl
     */
    public String getJsFileUrl() {
        return jsFileUrl;
    }

    /**
     * @param jsFileUrl the jsFileUrl to set
     */
    public void setJsFileUrl(String jsFileUrl) {
        this.jsFileUrl = jsFileUrl;
    }

    /**
     * @return the layoutName
     */
    public String getLayoutName() {
        return layoutName;
    }

    /**
     * @param layoutName the layoutName to set
     */
    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }
    
}
