package com.kiosk.profile.entity;

public enum  StoreActivityEnum {
    POST_ITEM,
    BUY_TOKEN,
    POST_TO_SOCIAL_MEDIA
}
