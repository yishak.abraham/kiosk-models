/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import com.kiosk.config.CheckNullCollection;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


/**
 *
 * @author TheStig
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "StoreOwner.findByFname",
            query = "SELECT so FROM StoreOwner so WHERE so.firstName = :fn")
    ,
    @NamedQuery(name = "StoreOwner.findStoreByEmail",
            query = "SELECT so FROM StoreOwner so WHERE LOWER(so.email.email) = LOWER(:email)"),
    @NamedQuery(name = "StoreOwner.FindStoreByStoreUrl",
            query = "SELECT so FROM StoreOwner so WHERE LOWER(so.store.storeUrl) = LOWER(:url)"),
    @NamedQuery(name = "StoreOwner.findPasswordByStoreUrl",
            query = "SELECT so.password FROM StoreOwner so WHERE so.store.storeUrl =:url")

})
public class StoreOwner implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "storeOwner_gen", sequenceName = "storeOwner_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "storeOwner_gen")
    private int id;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @NotNull
    @Size(min=6)
    @JsonbTransient
    private String password;
    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private Email email;
    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private Phone phone;
    @Enumerated(EnumType.STRING)
    private RegisteredBy registerdBy;
    @JsonbTransient
    @CheckNullCollection
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST)
    private Set<Role> roleCollection;
    @JsonbTransient
    @OneToOne(cascade = {CascadeType.PERSIST,CascadeType.REMOVE})
    private Store store;

    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the firstName
     */
    public String getFirtName() {
        return firstName;
    }

    /**
     * @param firtName the firstName to set
     */
    public void setFirstName(String firtName) {
        this.firstName = firtName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public Email getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(Email email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public Phone getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    /**
     * @return the chosen
     */
    public RegisteredBy getRegisterdBy() {
        return registerdBy;
    }

    /**
     * @param registerdBy

     */
    public void setRegisterdBy(RegisteredBy registerdBy) {
        this.registerdBy = registerdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StoreOwner)) {
            return false;
        }
        StoreOwner owner = (StoreOwner) o;
        return owner.id == this.id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id;
        return hash;
    }

    /**
     * @return the roleCollection
     */
    @JsonbTransient
    public Set<Role> getRoleCollection() {
        return roleCollection;
    }

    /**
     * @param roleCollection the roleCollection to set
     */
    @JsonbTransient
    public void setRoleCollection(Set<Role> roleCollection) {
        this.roleCollection = roleCollection;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
