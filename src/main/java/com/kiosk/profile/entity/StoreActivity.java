package com.kiosk.profile.entity;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class StoreActivity implements Serializable {

    @Id
    @SequenceGenerator(name = "StoreActivity_gen", sequenceName = "StoreActivity_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "StoreActivity_gen")
    private Long id;
    @JsonbTransient
    @ManyToOne(fetch = FetchType.LAZY)
    private Store StoreId;
    @ManyToOne
    private ActivityType activityType;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime activityDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Store getStoreId() {
        return StoreId;
    }

    public void setStoreId(Store storeId) {
        StoreId = storeId;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public LocalDateTime getActivityDate() {
        return activityDate;
    }

    public void setActivityDate(LocalDateTime activityDate) {
        this.activityDate = activityDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreActivity that = (StoreActivity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
