/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.profile.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;



@Entity
@NamedQueries({
    @NamedQuery(name = "Theme.FindAll",
            query = "SELECT t FROM Theme t")
})
public class Theme implements Serializable {
    
    @Id
    @SequenceGenerator(name = "theme_gen", sequenceName = "theme_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "theme_gen")
    private int id;
    @Size(max = 20)
    private String themeName;
    @Size(max = 50)
    private String description;
    private String image;
    private String themePath;
    private int tokenRquired;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the themeName
     */
    public String getThemeName() {
        return themeName;
    }

    /**
     * @param themeName the themeName to set
     */
    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * @param image the image to set
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * @return the themePath
     */
    public String getThemePath() {
        return themePath;
    }

    /**
     * @param themePath the themePath to set
     */
    public void setThemePath(String themePath) {
        this.themePath = themePath;
    }

    /**
     * @return the tokenRquired
     */
    public int getTokenRquired() {
        return tokenRquired;
    }

    /**
     * @param tokenRquired the tokenRquired to set
     */
    public void setTokenRquired(int tokenRquired) {
        this.tokenRquired = tokenRquired;
    }
}
