/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.entity;

import com.kiosk.profile.entity.CategoryEnum;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Size;

/**
 *
 * @author mmesfin
 */
@Embeddable
public class TagId implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Size(max = 20)
    private String tagName;
    @Enumerated(EnumType.STRING)
    private CategoryEnum category;
    
    public TagId(){}
    public TagId(String tagId, String category){
        this.tagName=tagId;
        this.category = CategoryEnum.valueOf(category);
        
    }

    /**
     * @return the tagId
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * @return the category
     */
    public CategoryEnum getCategory() {
        return category;
    }
    
        @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(!(obj instanceof TagId))
            return false;
        final TagId other = (TagId) obj;
        return other.tagName.equals(this.tagName) && other.category.equals(this.category);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.tagName);
        hash = 79 * hash + Objects.hashCode(this.category);
        return hash;
    } 
}
