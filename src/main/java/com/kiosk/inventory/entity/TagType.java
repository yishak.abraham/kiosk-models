/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.entity;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 *
 * @author mmesfin
 */
@Entity
public class TagType implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @Column(length=20)
	@Enumerated(EnumType.STRING)
    private TagTypeEnum tagTypeId;
    private String description;

    /**
     * @return the name
     */
    public String getName() {
        return tagTypeId.name();
    }

    /**
     * @param name the name to set
     */
    public TagType setName(TagTypeEnum name) {
        this.tagTypeId = name;
        return this;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(!(obj instanceof TagType))
            return false;
        final TagType other = (TagType) obj;
        return other.tagTypeId.equals(this.tagTypeId);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.tagTypeId);
        return hash;
    }
    
}
