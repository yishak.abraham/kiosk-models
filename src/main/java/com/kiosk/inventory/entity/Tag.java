/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.entity;

import com.kiosk.profile.entity.Category;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author mmesfin
 */
@NamedQueries({
//    @NamedQuery(name = "Tag.findByCategory",
//            query = "SELECT t FROM Tag t where t.category:=category")
})
@Table(
    uniqueConstraints=
        @UniqueConstraint(columnNames={"tagname", "category_name"})
)
@Entity
public class Tag implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @SequenceGenerator(name = "tag_gen", sequenceName = "tag_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "tag_gen")
    private int id;
    private String tagName;
    @OneToOne
    private TagType tagType;
    private String description;
    @ManyToOne
    private Category category;


    
    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(!(obj instanceof Tag))
            return false;
        final Tag other = (Tag) obj;
        return other.getId() == this.getId();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.tagName);
        return hash;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return the tags
     */
    public TagType getTagType() {
        return tagType;
    }

    /**
     * @param tagType the tags to set
     */
    public void setTagType(TagType tagType) {
        this.tagType = tagType;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return the tagName
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * @param tagName the tagName to set
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
