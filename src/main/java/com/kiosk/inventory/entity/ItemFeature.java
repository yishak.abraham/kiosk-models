package com.kiosk.inventory.entity;

import java.io.Serializable;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class ItemFeature implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name = "feature_gen", sequenceName = "feature_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "feature_gen")
	private int id;
	private String name;
	private String value;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonbTransient
	private Item item;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
