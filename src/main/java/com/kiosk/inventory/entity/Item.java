/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.entity;

//import com.kiosk.inventory.JsonConverter;

import com.kiosk.image.Image;
import com.kiosk.profile.entity.Branch;
import com.kiosk.profile.entity.Category;
import com.kiosk.profile.entity.Store;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

/**
 *
 * @author mmesfin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i"),
    @NamedQuery(name = "Item.findStoreById", query = "SELECT i FROM Item i WHERE i.store.storeId =:storeId")})
public class Item implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "item_gen", sequenceName = "item_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "item_gen")
    private int id;
    private String name;
    @Column(columnDefinition = "tsvector",insertable = false,updatable = false)
    private String itemNameToken;
    private double price;
    @OneToOne
    private Category category;
    @Column(length=12800)
    private String description;
    private boolean discount;
    private Integer quantity;
    private double discountAmount;
    @ManyToOne
    private Store store;
    @OneToMany(fetch = FetchType.EAGER,cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @Fetch(FetchMode.SUBSELECT)
    private final List<Image> images;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "item", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private final Set<ItemFeature> features;
    private String brand;
    private String condition;
    private String review;
    private int token;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime addedDate;
    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime updatedDate;
    private boolean posted;
    @ManyToMany(fetch = FetchType.EAGER)
    private final Set<Branch> branch;
    private boolean isOnMainBranch;

    public Item() {
        this.features = new HashSet<>();
        this.images = new ArrayList<>();
        this.branch = new HashSet<>();
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the category
     */
    public Category getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the discount
     */
    public boolean isDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(boolean discount) {
        this.discount = discount;
    }

    /**
     * @return the discountAmount
     */
    public double getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }

    /**
     * @return the images
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * @param images the images to set
     */
    public void setImages(Image images) {
        this.images.add(images);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Item)) {
            return false;
        }
        final Item other = (Item) obj;
        return this.id == other.id;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + this.id;
        return hash;
    }

    /**
     * @return the review
     */
    public String getReview() {
        return review;
    }

    /**
     * @param review the review to set
     */
    public void setReview(String review) {
        this.review = review;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LocalDateTime getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDateTime addedDate) {
        this.addedDate = addedDate;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Set<ItemFeature> getFeatures() {
        return features;
    }

    /**
     * @return the posted
     */
    public boolean isPosted() {
        return posted;
    }

    /**
     * @param posted the posted to set
     */
    public void setPosted(boolean posted) {
        this.posted = posted;
    }

    /**
     * @return the branch
     */
    public Collection<Branch> getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(Branch branch) {
        this.branch.add(Objects.requireNonNull(branch, "can't set a non existing branch to item"));
    }

    /**
     * @return the isOnMainBranch
     */
    public boolean isIsOnMainBranch() {
        return isOnMainBranch;
    }

    /**
     * @param isOnMainBranch the isOnMainBranch to set
     */
    public void setIsOnMainBranch(boolean isOnMainBranch) {
        this.isOnMainBranch = isOnMainBranch;
    }

    /**
     * @return the updatedDate
     */
    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    /**
     * @param updatedDate the updatedDate to set
     */
    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getNameToken() {
        return itemNameToken;
    }

    public void setNameToken(String itemNameToken) {
        this.itemNameToken = itemNameToken;
    }
}
