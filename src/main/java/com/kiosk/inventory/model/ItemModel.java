/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.model;

import com.kiosk.image.ImageUploadModel;

import javax.enterprise.context.Dependent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mmesfin
 */
@Dependent
public class ItemModel {

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

//    private JsonObject features;
    @NotEmpty
    private String name;
    @NotNull
    private Double price;
    @NotEmpty
    private String category;
    private Integer quantity;
    private String condition;
    private String brand;
    private String description;
    private Integer token;
    @NotEmpty
    private List<ImageUploadModel> images;
    Map<String, String> features;
    private boolean posted;
    private List<Integer> branch;
    private boolean onMainBranch;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public List<ImageUploadModel> getImages() {
        return images;
    }

    public void setImages(List<ImageUploadModel> images) {
        this.images = images;
    }

    public Map<String, String> getFeatures() {
        return features;
    }

    public void setFeatures(Map<String, String> features) {
        this.features = features;
    }

    /**
     * @return the posted
     */
    public boolean isPosted() {
        return posted;
    }

    /**
     * @param posted the posted to set
     */
    public void setPosted(boolean posted) {
        this.posted = posted;
    }

    /**
     * @return the branch
     */
    public List<Integer> getBranch() {
        return branch;
    }

    /**
     * @param branch the branch to set
     */
    public void setBranch(List<Integer> branch) {
        this.branch = branch;
    }

    /**
     * @return the onMainBranch
     */
    public boolean isOnMainBranch() {
        return onMainBranch;
    }

    /**
     * @param onMainBranch the onMainBranch to set
     */
    public void setOnMainBranch(boolean onMainBranch) {
        this.onMainBranch = onMainBranch;
    }
}
