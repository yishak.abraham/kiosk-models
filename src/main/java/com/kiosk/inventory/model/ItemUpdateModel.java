/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.model;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import javax.json.JsonArray;
import javax.json.JsonString;
import javax.json.JsonValue;

/**
 *
 * @author mmesfin
 */
public class ItemUpdateModel {


    /**
     * @param features the features to set
     */
    public void setFeatures(JsonArray features) {
//        final List<Map.Entry<String, JsonValue>> collect = features.stream().flatMap(e -> e.asJsonObject().entrySet().stream())
//                .collect(toList());    
//            this.features =  Optional.ofNullable(collect.stream()
//                .collect(toMap(e -> e.getKey(),e -> e.getValue().toString())));
        final List<Entry<String, JsonValue>> collect1 = features.stream().flatMap(e -> e.asJsonObject().entrySet().stream())
                .collect(toList());    
        final Map<String, String> collect2 = collect1.stream()
                .collect(toMap(e -> e.getKey()
                        ,e -> ((JsonString)e.getValue()).getString()));
        this.features = Optional.ofNullable(collect2);
    }

    /**
     * @return the features
     */
    public Optional<Map<String, String>> getFeatures() {
        return features;
    }

    /**
     * @return the name
     */
    public Optional<String> getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(Optional<String> name) {
        this.name = name;
    }

    /**
     * @return the price
     */
    public Optional<Double> getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Optional<Double> price) {
        this.price = price;
    }

    /**
     * @return the category
     */
    public Optional<String> getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Optional<String> category) {
        this.category = category;
    }

    /**
     * @return the quantity
     */
    public Optional<Integer> getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Optional<Integer> quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the condition
     */
    public Optional<String> getCondition() {
        return condition;
    }

    /**
     * @param condition the condition to set
     */
    public void setCondition(Optional<String> condition) {
        this.condition = condition;
    }

    /**
     * @return the brand
     */
    public Optional<String> getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(Optional<String> brand) {
        this.brand = brand;
    }

    /**
     * @return the description
     */
    public Optional<String> getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(Optional<String> description) {
        this.description = description;
    }

    /**
     * @return the itemId
     */
    public Integer getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    private Optional<String> name;
    private Optional<Double> price;
    private Optional<String> category;
    private Optional<Integer> quantity;
    private Optional<String> condition;
    private Optional<String> brand;
    private Optional<String> description;
    private Optional<Map<String, String>> features;
    private int token;
    private Integer itemId;
    private Optional<Set<Integer>> newBranch;
    private Optional<Set<Integer>> deleteBranch;
    private Optional<Boolean> onMainBranch;

    /**
     * @return the token
     */
    public int getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(int token) {
        this.token = token;
    }

    /**
     * @return the newBranch
     */
    public Optional<Set<Integer>> getNewBranch() {
        return newBranch;
    }

    /**
     * @param newBranch the newBranch to set
     */
    public void setNewBranch(Optional<Set<Integer>> newBranch) {
        this.newBranch = newBranch;
    }

    /**
     * @return the deleteBranch
     */
    public Optional<Set<Integer>> getDeleteBranch() {
        return deleteBranch;
    }

    /**
     * @param deleteBranch the deleteBranch to set
     */
    public void setDeleteBranch(Optional<Set<Integer>> deleteBranch) {
        this.deleteBranch = deleteBranch;
    }

    /**
     * @return the onMainBranch
     */
    public Optional<Boolean> getOnMainBranch() {
        return onMainBranch;
    }

    /**
     * @param onMainBranch the onMainBranch to set
     */
    public void setOnMainBranch(Optional<Boolean> onMainBranch) {
        this.onMainBranch = onMainBranch;
    }


}
