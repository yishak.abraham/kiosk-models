/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.model;

import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 *
 * @author mmesfin
 */
public enum ItemAddKeys {
    name,
    price,
    category,
    quantity,
    condition,
    brand,
    description,
    token,
    posted,
    onMainBranch;
    
    public static Supplier<Stream<String>> getKeys = () -> Arrays.stream(ItemAddKeys.values()).map(String::valueOf);
}
