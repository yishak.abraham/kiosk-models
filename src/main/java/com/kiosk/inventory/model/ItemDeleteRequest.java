/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.inventory.model;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author mmesfin
 */
public class ItemDeleteRequest {
    private List<Integer> imageId;
    private int itemId;
    private Optional<Integer> mainImage;


    /**
     * @return the itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * @param itemId the itemId to set
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /**
     * @return the imageId
     */
    public List<Integer> getImageId() {
        return imageId;
    }

    /**
     * @param imageId the imageId to set
     */
    public void setImageId(List<Integer> imageId) {
        this.imageId = imageId;
    }

    /**
     * @return the mainImage
     */
    public Optional<Integer> getMainImage() {
        return mainImage;
    }

    /**
     * @param mainImage the mainImage to set
     */
    public void setMainImage(Optional<Integer> mainImage) {
        this.mainImage = mainImage;
    }
}
