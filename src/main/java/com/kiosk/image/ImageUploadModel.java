package com.kiosk.image;

import java.io.InputStream;

public class ImageUploadModel {

    private String extension;
    private InputStream inputStream;
    private boolean isMain;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * @return the isMain
     */
    public boolean isIsMain() {
        return isMain;
    }

    /**
     * @param isMain the isMain to set
     */
    public void setIsMain(boolean isMain) {
        this.isMain = isMain;
    }

}
