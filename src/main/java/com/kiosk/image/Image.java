/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.image;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;

/**
 *
 *
 * @author mmesfin
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Image.DeleteByList",
            query = "DELETE FROM Image i where i.id IN :ids")
})
public class Image implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "image_gen", sequenceName = "image_seq",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "image_gen")
    private int id;
    @Column(unique = true)
    private String name;
    private String path;
    private boolean isMain;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Image)) {
            return false;
        }
        final Image other = (Image) obj;
        return this.name.equals(other.name);

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    /**
     * @return the isMain
     */
    public boolean isIsMain() {
        return isMain;
    }

    /**
     * @param isMain the isMain to set
     */
    public void setIsMain(boolean isMain) {
        this.isMain = isMain;
    }
}
