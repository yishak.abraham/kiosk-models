/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.image;

import javax.enterprise.context.Dependent;

/**
 *
 * @author TheStig
 */
@Dependent
public class ImageModel {
    private byte[] thumbnails;
    private String name;



    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the thumbnails
     */
    public byte[] getThumbnails() {
        return thumbnails;
    }

    /**
     * @param thumbnails the thumbnails to set
     */
    public void setThumbnails(byte[] thumbnails) {
        this.thumbnails = thumbnails;
    }
    
    
}
