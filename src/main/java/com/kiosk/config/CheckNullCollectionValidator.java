/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.config;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Collection;

/**
 *
 * @author TheStig
 */
public class CheckNullCollectionValidator implements ConstraintValidator<CheckNullCollection,Collection>{

    @Override
    public void initialize(CheckNullCollection a) {
       // ConstraintValidator.super.initialize(a);
    }

    @Override
    public boolean isValid(Collection t, ConstraintValidatorContext cvc) {
       return t.stream().noneMatch((e) -> (e == null));
    }
    
}
