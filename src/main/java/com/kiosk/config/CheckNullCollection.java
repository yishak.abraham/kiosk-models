/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kiosk.config;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 *
 * @author TheStig
 */
@Documented
@Constraint(validatedBy = CheckNullCollectionValidator.class)
@Target({ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface CheckNullCollection {

    String message() default "collection element cannot be null";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

   //ArrayList type();

    
}
